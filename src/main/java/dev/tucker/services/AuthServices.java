package dev.tucker.services;

import dev.tucker.data.AuthDAO;
import dev.tucker.hibdata.AuthDaoHibImp;
import dev.tucker.models.Member;

public class AuthServices {

    AuthDAO authDAOImp = new AuthDaoHibImp();

    public int checkAdminCredentials(String userName, String password) {
        return authDAOImp.checkAdminCredentials(userName, password);
    }

    public int checkUserCredentials(String email, String password) {
        return authDAOImp.checkUserCredentials(email, password);
    }

    public void add(Member user) {
        authDAOImp.addNewUser(user);
    }
}
