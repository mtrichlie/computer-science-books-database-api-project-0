package dev.tucker.util;

import io.javalin.http.Context;

public class SecurityUtil {

    public void attachResponseHeaders(Context ctx) {
        ctx.header("access-control-expose-headers", "Authorization");

    }
}
