package dev.tucker.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@DynamicUpdate
@JsonIgnoreProperties("hibernateLazyInitializer")
public class Book implements Serializable {

    @Id
    private String title;
    private String publisher;
    private String date;
    private String isbn;
    @Lob
    private String description;
    private double rating;

    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(
            name="book_genre",
            joinColumns={@JoinColumn(name="title")},
            inverseJoinColumns={@JoinColumn(name="genreTitle")})
    private List<Genre> genres = new ArrayList<>();

    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(
            name="book_author",
            joinColumns={@JoinColumn(name="title")},
            inverseJoinColumns={@JoinColumn(name="name")})
    private List<Author> authors = new ArrayList<>();

    // List<String> genres = new ArrayList<>();
    // List<String> authors = new ArrayList<>();

    public Book() {
        // no-args constructor
        super();
    }

    public Book(String title) {
        this.title = title;
    }

    public Book(String title, String description, String publisher) {
        this.title = title;
        this.description = description;
        this.publisher = publisher;
    }

    public Book(String title, List<Genre> genres) {
        this.title = title;
        this.genres = genres;
    }


    public Book(String title, String publisher, String date,
                String isbn, String description) {
        this.title = title;
        this.publisher = publisher;
        this.date = date;
        this.isbn = isbn;
        this.description = description;
        this.rating = 0;

    }

    public Book(String title, String publisher, String date,
                String isbn, String description, double rating) {
        this.title = title;
        this.publisher = publisher;
        this.date = date;
        this.isbn = isbn;
        this.description = description;
        this.rating = rating;

    }

    public Book(String title, String publisher, String date,
                String isbn, String description, double rating, List<Genre> genres, List<Author> authors) {
        this.title = title;
        this.publisher = publisher;
        this.date = date;
        this.isbn = isbn;
        this.description = description;
        this.rating = rating;
        this.genres = genres;
        this.authors = authors;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void addGenres(Genre genre) {
        genres.add(genre);
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void addAuthor(Author author) {
        authors.add(author);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return Double.compare(book.rating, rating) == 0 && Objects.equals(title, book.title) && Objects.equals(publisher, book.publisher) && Objects.equals(date, book.date) && Objects.equals(isbn, book.isbn) && Objects.equals(description, book.description) && Objects.equals(genres, book.genres) && Objects.equals(authors, book.authors);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, publisher, date, isbn, description, rating, genres, authors);
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", publisher='" + publisher + '\'' +
                ", date='" + date + '\'' +
                ", isbn='" + isbn + '\'' +
                ", description='" + description + '\'' +
                ", rating=" + rating +
                ", genres=" + genres +
                ", authors=" + authors +
                '}';
    }
}
