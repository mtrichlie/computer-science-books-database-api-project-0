package dev.tucker.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;


@Entity
public class Admin implements Serializable {


    private String passWord;
    private String userName;

    @Id
    @ManyToOne
    @JoinColumn(name="email")
    private Member user;


    public Admin() {
        super();
    }

    public Admin(String name, String email, String passWord, Member user) {
        this.passWord = passWord;
        this.userName = userName;
        this.user = user;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Admin admin = (Admin) o;
        return Objects.equals(passWord, admin.passWord);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), passWord);
    }

    @Override
    public String toString() {
        return "Admin{" +
                "passWord='" + passWord + '\'' +
                '}';
    }
}
