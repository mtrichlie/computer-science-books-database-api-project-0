package dev.tucker.data;

import dev.tucker.models.Book;
import dev.tucker.util.ConnectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/*public class BookDAOImp implements BookDAO{

    Logger logger = LoggerFactory.getLogger(BookDAOImp.class);

    @Override
    public List<Book> getAllBooks() {

        List<Book> books = new ArrayList<>();

        try(Connection connection = ConnectionUtil.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from book");

            while (resultSet.next()) {
                String title = resultSet.getString("title");
                String publisher = resultSet.getString("publisher");
                String date = resultSet.getString("date");
                String isbn = resultSet.getString("isbn");
                String description = resultSet.getString("description");
                double rating = resultSet.getDouble("average_rating");
                Book book = new Book(title, publisher, date, isbn, description, rating);
                PreparedStatement preparedStatement = connection.prepareStatement("select * from book_genre where book_title = ?");
                preparedStatement.setString(1, title);
                ResultSet resultSet2 = preparedStatement.executeQuery();
                PreparedStatement preparedStatement2 = connection.prepareStatement("select * from author where book_title = ?");
                preparedStatement2.setString(1, title);
                ResultSet resultSet3 = preparedStatement2.executeQuery();
                while (resultSet2.next()) {
                    String genre = resultSet2.getString("genre_title");
                    book.addGenres(genre);
                }
                while (resultSet3.next()) {
                    String author = resultSet3.getString("author");
                    book.addAuthor(author);
                }

                books.add(book);
            }
            logger.info("Retrieving " + books.size() + " books");
        } catch (SQLException e) {
            logger.error(e.getClass()+ " " +e.getMessage());
        }

        return books;
    }

    @Override
    public Book getBookInfoByTitle(String title) {


        try (Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement getBookTitle = connection.prepareStatement("select * from book where title = ?");
            getBookTitle.setString(1, title);
            ResultSet resultSet = getBookTitle.executeQuery();


            if (resultSet.next()) {

                String publisher = resultSet.getString("publisher");
                String date = resultSet.getString("date");
                String isbn = resultSet.getString("isbn");
                String description = resultSet.getString("description");
                double rating = resultSet.getDouble("average_rating");
                Book book = new Book(title, publisher, date, isbn, description, rating);
                PreparedStatement preparedStatement = connection.prepareStatement("select * from book_genre where book_title = ?");
                preparedStatement.setString(1, title);
                ResultSet resultSet2 = preparedStatement.executeQuery();
                PreparedStatement preparedStatement2 = connection.prepareStatement("select * from author where book_title = ?");
                preparedStatement2.setString(1, title);
                ResultSet resultSet3 = preparedStatement2.executeQuery();
                while (resultSet2.next()) {
                    String genre = resultSet2.getString("genre_title");
                    book.addGenres(genre);
                }
                while (resultSet3.next()) {
                    String author = resultSet3.getString("author");
                    book.addAuthor(author);
                }
                return book;

            }

        } catch (SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }
        return null;
    }

    @Override
    public Book getBookInfoByIsbn(String isbn) {
        try (Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement getBookTitle = connection.prepareStatement("select * from book where isbn = ?");
            getBookTitle.setString(1, isbn);
            ResultSet resultSet = getBookTitle.executeQuery();


            if (resultSet.next()) {
                String title = resultSet.getString("title");
                String publisher = resultSet.getString("publisher");
                String date = resultSet.getString("date");
                String description = resultSet.getString("description");
                double rating = resultSet.getDouble("average_rating");
                Book book = new Book(title, publisher, date, isbn, description, rating);
                PreparedStatement preparedStatement = connection.prepareStatement("select * from book_genre where book_title = ?");
                preparedStatement.setString(1, title);
                ResultSet resultSet2 = preparedStatement.executeQuery();
                PreparedStatement preparedStatement2 = connection.prepareStatement("select * from author where book_title = ?");
                preparedStatement2.setString(1, title);
                ResultSet resultSet3 = preparedStatement2.executeQuery();
                while (resultSet2.next()) {
                    String genre = resultSet2.getString("genre_title");
                    book.addGenres(genre);
                }
                while (resultSet3.next()) {
                    String author = resultSet3.getString("author");
                    book.addAuthor(author);
                }
                return book;

            }

        } catch (SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }

        return null;
    }

    @Override
    public List<Book> getBooksByPublisher(String publisher) {

        List<Book> books = new ArrayList<>();

        try (Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement getBooksByPublisher = connection.prepareStatement("select * from book where publisher = ?");
            getBooksByPublisher.setString(1, publisher);
            ResultSet resultSet = getBooksByPublisher.executeQuery();

            while (resultSet.next()) {
                String title = resultSet.getString("title");
                String date = resultSet.getString("date");
                String isbn = resultSet.getString("isbn");
                String description = resultSet.getString("description");
                double rating = resultSet.getDouble("average_rating");
                Book book = new Book(title, publisher, date, isbn, description, rating);
                PreparedStatement preparedStatement = connection.prepareStatement("select * from book_genre where book_title = ?");
                preparedStatement.setString(1, title);
                ResultSet resultSet2 = preparedStatement.executeQuery();
                PreparedStatement preparedStatement2 = connection.prepareStatement("select * from author where book_title = ?");
                preparedStatement2.setString(1, title);
                ResultSet resultSet3 = preparedStatement2.executeQuery();
                while (resultSet2.next()) {
                    String genre = resultSet2.getString("genre_title");
                    book.addGenres(genre);
                }
                while (resultSet3.next()) {
                    String author = resultSet3.getString("author");
                    book.addAuthor(author);
                }

                books.add(book);
            }
            logger.info("Retrieving " + books.size() + " books");
        } catch (SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }

        return books;
    }

    @Override
    public List<Book> getBooksByGenre(String genre) {

        List<Book> books = new ArrayList<>();

        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement joinTables = connection.prepareStatement("select b.title, b.publisher, b.date, b.isbn, b.description, b.average_rating, bg.genre_title from book b join book_genre bg on b.title = bg.book_title where bg.genre_title = ?");
            joinTables.setString(1, genre);
            ResultSet resultSet = joinTables.executeQuery();

            while (resultSet.next()) {
                logger.info("Checking");
                String title = resultSet.getString("title");
                String publisher = resultSet.getString("publisher");
                String date = resultSet.getString("date");
                String isbn = resultSet.getString("isbn");
                String description = resultSet.getString("description");
                double rating = resultSet.getDouble("average_rating");
                Book book = new Book(title, publisher, date, isbn, description, rating);
                PreparedStatement preparedStatement = connection.prepareStatement("select * from book_genre where book_title = ?");
                preparedStatement.setString(1,title);
                ResultSet resultSet2 = preparedStatement.executeQuery();
                PreparedStatement preparedStatement2 = connection.prepareStatement("select * from author where book_title = ?");
                preparedStatement2.setString(1, title);
                ResultSet resultSet3 = preparedStatement2.executeQuery();
                while (resultSet2.next()) {
                    String genreTitle = resultSet2.getString("genre_title");
                    book.addGenres(genreTitle);
                }
                while (resultSet3.next()) {
                    String author = resultSet3.getString("author");
                    book.addAuthor(author);
                }

                books.add(book);
            }
            logger.info("Retrieving " + books.size() + " books");
        }
        catch(SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }
        return books;
    }

    @Override
    public List<Book> getBooksByAuthor(String author) {
        List<Book> books = new ArrayList<>();

        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement joinTables = connection.prepareStatement("select b.title, b.publisher, b.date, b.isbn, b.description, b.average_rating, a.author from book b join author a on b.title = a.book_title where a.author = ?");
            joinTables.setString(1, author);
            ResultSet resultSet = joinTables.executeQuery();

            while (resultSet.next()) {
                logger.info("Checking");
                String title = resultSet.getString("title");
                String publisher = resultSet.getString("publisher");
                String date = resultSet.getString("date");
                String isbn = resultSet.getString("isbn");
                String description = resultSet.getString("description");
                double rating = resultSet.getDouble("average_rating");
                Book book = new Book(title, publisher, date, isbn, description, rating);
                PreparedStatement preparedStatement = connection.prepareStatement("select * from book_genre where book_title = ?");
                preparedStatement.setString(1,title);
                ResultSet resultSet2 = preparedStatement.executeQuery();
                PreparedStatement preparedStatement2 = connection.prepareStatement("select * from author where book_title = ?");
                preparedStatement2.setString(1, title);
                ResultSet resultSet3 = preparedStatement2.executeQuery();
                while (resultSet2.next()) {
                    String genreTitle = resultSet2.getString("genre_title");
                    book.addGenres(genreTitle);
                }
                while (resultSet3.next()) {
                    String anAuthor = resultSet3.getString("author");
                    book.addAuthor(anAuthor);
                }

                books.add(book);
            }
            logger.info("Retrieving " + books.size() + " books");
        }
        catch(SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }
        return books;
    }

    @Override
    public void addNewBook(Book book) {

        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("insert into book values (?, ?, ?, ?, ?, ?)");
            PreparedStatement preparedStatement2 = connection.prepareStatement("insert into book_genre values (?, ?)");
            PreparedStatement preparedStatement3 = connection.prepareStatement("insert into author values (?, ?)");
            preparedStatement.setString(1, book.getTitle());
            preparedStatement.setString(2, book.getPublisher());
            preparedStatement.setString(3, book.getDate());
            preparedStatement.setString(4, book.getIsbn());
            preparedStatement.setString(5, book.getDescription());
            preparedStatement.setDouble(6, book.getRating());
            preparedStatement.executeUpdate();
            for (String i : book.getGenres()) {
                preparedStatement2.setString(1, book.getTitle());
                preparedStatement2.setString(2, i);
                preparedStatement2.executeUpdate();
            }
            for (String i : book.getAuthors()) {
                preparedStatement3.setString(1, i);
                preparedStatement3.setString(2, book.getTitle());
                preparedStatement3.executeUpdate();
            }
            logger.info("Books added successfully");

        } catch (SQLException e) {
            logger.error(e.getClass()+ " " +e.getMessage());
        }

    }

    @Override
    public void deleteBook(String isbn) {
        try(Connection connection = ConnectionUtil.getConnection()){

            List<PreparedStatement> preparedStatementList = new ArrayList<>();

            PreparedStatement preparedStatement = connection.prepareStatement("select title from book where isbn = ?");
            preparedStatement.setString(1, isbn);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {

                PreparedStatement preparedStatement2 = connection.prepareStatement("delete from book_genre where book_title = ?");
                PreparedStatement preparedStatement3 = connection.prepareStatement("delete from book_review where book_title = ?");
                PreparedStatement preparedStatement4 = connection.prepareStatement("delete from book_wishlist where book_title = ?");
                PreparedStatement preparedStatement5 = connection.prepareStatement("delete from author where book_title = ?");
                PreparedStatement preparedStatement6 = connection.prepareStatement("delete from book where isbn = ?");


                preparedStatementList.add(preparedStatement2);
                preparedStatementList.add(preparedStatement3);
                preparedStatementList.add(preparedStatement4);
                preparedStatementList.add(preparedStatement5);
                preparedStatementList.add(preparedStatement6);

                for (PreparedStatement i : preparedStatementList) {
                    if (i == preparedStatement6) {
                        preparedStatement6.setString(1, isbn);
                        preparedStatement6.executeUpdate();
                    } else {
                        logger.info("" + resultSet.getString("title"));
                        i.setString(1, resultSet.getString("title"));
                        i.executeUpdate();
                    }
                }

            }

            logger.info("Book with isbn number: " + isbn + " deleted from database");

        } catch (SQLException e) {
            logger.error(e.getClass()+ " " +e.getMessage());
        }

    }

    @Override
    public void updateBookDescription(String isbn, String description) {
        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("update book set description = ? where isbn = ?");
            preparedStatement.setString(1, description);
            preparedStatement.setString(2, isbn);
            preparedStatement.executeUpdate();
        } catch(SQLException e) {
            logger.error(e.getClass()+ " " +e.getMessage());
        }

    }

    @Override
    public void updateBookPublisher(String isbn, String publisher) {
        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("update book set publisher = ? where isbn = ?");
            preparedStatement.setString(1, publisher);
            preparedStatement.setString(2, isbn);
            preparedStatement.executeUpdate();
        } catch(SQLException e) {
            logger.error(e.getClass()+ " " +e.getMessage());
        }
    }

    @Override
    public void updateBookGenres(String title, String genre) {
        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("insert into book_genre values (?, ?)");
            preparedStatement.setString(1, title);
            preparedStatement.setString(2, genre);
            preparedStatement.executeUpdate();
        } catch(SQLException e) {
            logger.error(e.getClass()+ " " +e.getMessage());
        }
    }

} */
