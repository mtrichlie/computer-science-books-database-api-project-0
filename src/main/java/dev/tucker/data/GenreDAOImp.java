package dev.tucker.data;


import dev.tucker.models.Genre;
import dev.tucker.util.ConnectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/*public class GenreDAOImp implements GenreDAO {

    Logger logger = LoggerFactory.getLogger(GenreDAOImp.class);

    @Override
    public List<Genre> getAllGenreDescriptions() {

        List<Genre> genres = new ArrayList<>();
        try (Connection connection = ConnectionUtil.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from genre");

            while (resultSet.next()) {
                String genreTitle = resultSet.getString("genre_title");
                String description = resultSet.getString("description");
                Genre genre = new Genre(genreTitle, description);
                genres.add(genre);
                PreparedStatement preparedStatement = connection.prepareStatement("select * from book_genre where genre_title = ?");
                preparedStatement.setString(1, genreTitle);
                ResultSet resultSet2 = preparedStatement.executeQuery();
                while (resultSet2.next()) {
                    String bookTitle = resultSet2.getString("book_title");
                    genre.addToBookList(bookTitle);
                }
            }
        } catch (SQLException e) {
            logger.error(e.getClass()+ " " +e.getMessage());
        }
        return genres;
    }

    @Override
    public void addNewGenre(Genre genre) {

        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("insert into genre values (?, ?)");
            preparedStatement.setString(1, genre.getGenreTitle());
            preparedStatement.setString(2, genre.getDescription());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getClass()+ " " +e.getMessage());
        }


    }

    @Override
    public void deleteGenre(String genreName) {
        try(Connection connection = ConnectionUtil.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement("delete from book_genre where genre_title = ?");
            preparedStatement.setString(1, genreName);
            preparedStatement.executeUpdate();

            PreparedStatement preparedStatement2 = connection.prepareStatement("delete from genre where genre_title = ?");
            preparedStatement2.setString(1, genreName);
            preparedStatement2.executeUpdate();

            logger.info("Deleting genre entry: " + genreName);
        } catch (SQLException e) {
            logger.error(e.getClass()+ " " +e.getMessage());
        }
    }

    @Override
    public void updateGenreDescription(Genre genre) {
        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("update genre set description = ? where genre_title = ?");
            preparedStatement.setString(1, genre.getDescription());
            preparedStatement.setString(2, genre.getGenreTitle());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getClass()+ " " +e.getMessage());
        }
    }
}*/
