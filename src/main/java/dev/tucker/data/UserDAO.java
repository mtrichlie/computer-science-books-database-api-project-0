package dev.tucker.data;

import dev.tucker.models.BookReview;
import dev.tucker.models.Member;
import dev.tucker.models.WishlistItem;

import java.util.List;

public interface UserDAO {

    public List<Member> getAllUsers();
    public Member getUser(String email);
    public List<BookReview> getBookReviewsByUserId(String email);
    public List<WishlistItem> getWishListByUserId(String email);
    public void addNewBookReview(BookReview bookReview);
    public void addNewBookToWishList(WishlistItem wishlistItem);
    public void deleteUser(String email);
    public void updateUserName(Member user);
    public void updateHasRead(WishlistItem wishlistItem);
    public List<String> getUserEmails();
}
