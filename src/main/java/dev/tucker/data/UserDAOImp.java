package dev.tucker.data;


/*public class UserDAOImp implements UserDAO {

    Logger logger = LoggerFactory.getLogger(UserDAOImp.class);

    @Override
    public List<User> getAllUsers() {

        List<User> users = new ArrayList<>();

        try (Connection connection = ConnectionUtil.getConnection()){
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from site_user");
            while (resultSet.next()) {
                String name = resultSet.getString("name");
                String email = resultSet.getString("email");
                User user = new User(name, email);

                PreparedStatement preparedStatement = connection.prepareStatement("select book_title, user_rating from book_review where email = ?");
                preparedStatement.setString(1, email);
                ResultSet resultSet2 = preparedStatement.executeQuery();
                while (resultSet2.next()) {
                    String bookTitle = resultSet2.getString("book_title");
                    double rating = resultSet2.getDouble("user_rating");
                    BookReview bookReview = new BookReview(bookTitle, rating, email);
                    user.addBookReview(bookReview);
                }

                PreparedStatement preparedStatement2 = connection.prepareStatement("select book_title, has_read from book_wishlist where email = ?");
                preparedStatement2.setString(1, email);
                ResultSet resultSet3 = preparedStatement2.executeQuery();
                while (resultSet3.next()) {
                    String bookTitle = resultSet3.getString("book_title");
                    boolean hasRead = resultSet3.getBoolean("has_read");
                    WishlistItem wishlistItem = new WishlistItem(bookTitle, hasRead, email);
                    user.addWishlistItem(wishlistItem);
                }

                users.add(user);
            }
            logger.info("Fetching users successful");
        } catch (SQLException e) {
            logger.error(e.getClass()+ " " +e.getMessage());
        }

        return users;
    }

    @Override
    public User getUser(String email) {

        User user = new User();
        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement userBookReviews = connection.prepareStatement("select * from site_user where email = ?");
            userBookReviews.setString(1, email);
            ResultSet resultSet = userBookReviews.executeQuery();

            if (resultSet.next()) {
                String name = resultSet.getString("name");
                user.setName(name);
                user.setEmail(email);
            }
        } catch(SQLException e) {
            logger.error(e.getClass()+ " " +e.getMessage());
        }
        return user;
    }

    @Override
    public List<BookReview> getBookReviewsByUserId(String email) {

        List<BookReview> bookReviews = new ArrayList<>();

        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement userBookReviews = connection.prepareStatement("select * from book_review where email = ?");
            userBookReviews.setString(1, email);
            ResultSet resultSet = userBookReviews.executeQuery();

            while (resultSet.next()) {
                String title = resultSet.getString("book_title");
                double rating = resultSet.getDouble("user_rating");
                BookReview bookReview = new BookReview(title, rating, email);
                bookReviews.add(bookReview);
            }
        } catch(SQLException e) {
            logger.error(e.getClass()+ " " +e.getMessage());
        }
        return bookReviews;
    }

    @Override
    public List<WishlistItem> getWishListByUserId(String email) {
        List<WishlistItem> wishlistItems = new ArrayList<>();

        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement userWishlists = connection.prepareStatement("select * from book_wishlist where email = ?");
            userWishlists.setString(1, email);
            ResultSet resultSet = userWishlists.executeQuery();

            while (resultSet.next()) {
                String title = resultSet.getString("book_title");
                boolean hasRead = resultSet.getBoolean("has_read");
                WishlistItem wishlistItem = new WishlistItem(title, hasRead, email);
                wishlistItems.add(wishlistItem);
            }
        } catch(SQLException e) {
            logger.error(e.getClass()+ " " +e.getMessage());
        }
        return wishlistItems;
    }


    @Override
    public void addNewBookReview(BookReview bookReview) {
        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("insert into book_review values (?, ?, ?)");

            preparedStatement.setString(1, bookReview.getBook());
            preparedStatement.setDouble(2, bookReview.getRating());
            preparedStatement.setString(3, bookReview.getEmail());
            preparedStatement.executeUpdate();

            PreparedStatement preparedStatement1 = connection.prepareStatement("select avg(user_rating) from book_review where book_title = ?");
            preparedStatement1.setString(1, bookReview.getBook());
            ResultSet resultSet = preparedStatement1.executeQuery();
            if (resultSet.next()) {
                PreparedStatement preparedStatement2 = connection.prepareStatement("update book set average_rating = ? where title = ?");
                logger.info("" + resultSet.getDouble("avg"));
                preparedStatement2.setDouble(1, resultSet.getDouble("avg"));
                preparedStatement2.setString(2, bookReview.getBook());
                preparedStatement2.executeUpdate();
            }

        } catch (SQLException e) {
            logger.error(e.getClass()+ " " +e.getMessage());
        }
    }

    @Override
    public void addNewBookToWishList(WishlistItem wishlistItem) {
        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("insert into book_wishlist values (?, ?, ?)");

            preparedStatement.setString(1, wishlistItem.getBook());
            preparedStatement.setBoolean(2, wishlistItem.isHasRead());
            preparedStatement.setString(3, wishlistItem.getEmail());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            logger.error(e.getClass()+ " " +e.getMessage());
        }
    }


    @Override
    public void deleteUser(String email) {
        try(Connection connection = ConnectionUtil.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement("delete from book_review where email = ?");
            preparedStatement.setString(1, email);
            preparedStatement.executeUpdate();

            PreparedStatement preparedStatement2 = connection.prepareStatement("delete from book_wishlist where email = ?");
            preparedStatement2.setString(1, email);
            preparedStatement2.executeUpdate();

            PreparedStatement preparedStatement3 = connection.prepareStatement("delete from site_user where email = ?");
            preparedStatement3.setString(1, email);
            preparedStatement3.executeUpdate();

            logger.info("Deleting user: " + email);
        } catch (SQLException e) {
            logger.error(e.getClass()+ " " +e.getMessage());
        }

    }

    @Override
    public void updateUserName(User user) {
        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("update site_user set name = ? where email = ?");
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getEmail());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getClass()+ " " +e.getMessage());
        }
    }

    @Override
    public void updateHasRead(WishlistItem wishlistItem) {
        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("update book_wishlist set has_read = ? where email = ? and book_title = ?");
            preparedStatement.setBoolean(1, wishlistItem.isHasRead());
            preparedStatement.setString(2, wishlistItem.getEmail());
            preparedStatement.setString(3, wishlistItem.getBook());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getClass()+ " " +e.getMessage());
        }
    }

    @Override
    public List<String> getUserEmails() {
        List<String> emailList = new ArrayList<>();

        try(Connection connection = ConnectionUtil.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select email from site_user");
            while (resultSet.next()) {
                String email = resultSet.getString("email");
                emailList.add(email);
            }
            logger.info("Fetching users successful");
        } catch (SQLException e) {
            logger.error(e.getClass()+ " " +e.getMessage());
        }

        return emailList;

    }
}
*/