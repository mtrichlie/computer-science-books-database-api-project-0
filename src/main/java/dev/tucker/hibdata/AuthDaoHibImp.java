package dev.tucker.hibdata;

import dev.tucker.data.AuthDAO;
import dev.tucker.models.Member;
import dev.tucker.util.ConnectionUtil;
import dev.tucker.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AuthDaoHibImp implements AuthDAO {

    Logger logger = LoggerFactory.getLogger(AuthDaoHibImp.class);

    @Override
    public int checkAdminCredentials(String userName, String password) {
        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from Admin where userName = ? and passWord = ?");
            preparedStatement.setString(1, userName);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                    return 1;
            }

        } catch(SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }

        return 0;
    }


    @Override
    public int checkUserCredentials(String email, String password) {
        try(Connection connection = ConnectionUtil.getConnection()) {
            logger.info("Here");
            PreparedStatement preparedStatement = connection.prepareStatement("select * from Member where email = ? and password = ?");
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                    return 1;
            }
        } catch(SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }
        return 0;
    }

    @Override
    public void addNewUser(Member user) {
        try(Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            s.persist(user);
            tx.commit();
        }
    }
}
