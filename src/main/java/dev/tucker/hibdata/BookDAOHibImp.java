package dev.tucker.hibdata;

import dev.tucker.controllers.BookController;
import dev.tucker.data.BookDAO;
import dev.tucker.models.Book;
import dev.tucker.models.Genre;
import dev.tucker.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.util.List;

public class BookDAOHibImp implements BookDAO {

    Logger logger = LoggerFactory.getLogger(BookDAOHibImp.class);


    @Override
    public List<Book> getAllBooks() {
        try (Session s = HibernateUtil.getSession()) {
            List<Book> bookList = s.createQuery("from Book", Book.class).list();
            return bookList;
        }
    }

    //FIX ME
    @Override
    public Book getBookInfoByTitle(String title) {
        try (Session s = HibernateUtil.getSession()) {
            Book book = s.get(Book.class, title);
            logger.info("got book from the database with title: " + title);
            return book;
        }

    }

    @Override
    public Book getBookInfoByIsbn(String isbn) {
        try (Session s = HibernateUtil.getSession()) {
            Query q = s.createQuery("from Book where isbn=:isbn");
            q.setParameter("isbn", isbn);
            List<Book> qList = q.list();
            return qList.get(0);
        }
    }


    @Override
    public List<Book> getBooksByPublisher(String publisher) {
        try (Session s = HibernateUtil.getSession()) {
            Query q = s.createQuery("from Book where publisher = :publisher", Book.class);
            q.setParameter("publisher", publisher);
            List<Book> bookList = q.list();
            return bookList;
        }
    }


    // FIX ME
    @Override
    public List<Book> getBooksByGenre(Genre genre) {
        try (Session s = HibernateUtil.getSession()) {
            Query q = s.createQuery("select b from Book b join b.genres g where g.genreTitle = :genreTitle");
            q.setParameter("genreTitle", genre.getGenreTitle());
            List<Book> bookList = q.list();
            return bookList;
        }
    }


    // FIX ME
    @Override
    public List<Book> getBooksByAuthor(String author) {
        try (Session s = HibernateUtil.getSession()) {
            Query q = s.createQuery("select b from Book b join b.authors a where a.name = :name");
            q.setParameter("name", author);
            List<Book> bookList = q.list();
            return bookList;
        }
    }

    @Override
    public void addNewBook(Book book) {
        try (Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            s.persist(book);
            tx.commit();
        }
    }

    @Override
    public void deleteBook(String title) {
        try(Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            s.delete(new Book(title));
            tx.commit();
        }
}

    @Override
    public void updateBookDescription(Book book) {
        try(Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            Query q = s.createQuery("update Book b set b.description=:description where b.title=:title");
            q.setParameter("description", book.getDescription());
            q.setParameter("title", book.getTitle());
            q.executeUpdate();
            tx.commit();
        }
    }

    @Override
    public void updateBookPublisher(Book book) {
        try(Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            Query q = s.createQuery("update Book b set b.publisher=:publisher where b.title=:title");
            q.setParameter("publisher", book.getPublisher());
            q.setParameter("title", book.getTitle());
            q.executeUpdate();
            tx.commit();
        }
    }

    @Override
    public void updateBookGenres(Book book) {
        try(Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            Query q = s.createQuery("update Book b set b.genres=:genres where b.title=:title");
            q.setParameter("genres", book.getGenres());
            q.setParameter("title", book.getTitle());
            q.executeUpdate();
            tx.commit();
        }
    }
}
